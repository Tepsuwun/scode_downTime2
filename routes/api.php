<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/routes_url', function()
{
    return 'สวัสดีชาวโลก2';
});

Route::any('/welcome', 'WelcomeController@welcome_laravel');
Route::any('/testdatabase', 'WelcomeController@testdatabase');
Route::any('/testdatabase2', function () {
    $data = DB::select("SELECT * FROM user");
    return response()->json($data);
});
Route::get('/check-connect',function(){
    if(DB::connection()->getDatabaseName())
    {
        return "Yes! successfully connected to the DB: " . DB::connection()->getDatabaseName();
    }else{
        return 'Connection False !!';
    }
});

Route::any('/search_user', 'SearchController@user');
Route::any('/search_oneuser', 'SearchController@oneuser');
Route::any('/search_manager', 'SearchController@manager');
Route::any('/alluser', 'SearchController@alluser');
Route::any('/all_register', 'SearchController@all_register');

Route::any('/adduser', 'InsertController@adduser');
Route::any('/addmanager', 'InsertController@addmanager');

Route::any('/delete_user', 'DeleteController@delete_user');

Route::any('/edit_user', 'UpdateController@update_user');
Route::any('/update_register', 'UpdateController@update_register');
Route::any('/testUpload', 'UploadController@testUpload');
Route::any('/UploadImg', 'UploadController@uploadImg');

Route::group([
    'namespace' => 'login'
], function () {
    Route::any('/loginUser', 'LoginController@loginuser');
    Route::any('/loginAdmin', 'LoginController@loginadmin');
});

Route::group([
    'namespace' => 'upload'
], function () {
    Route::any('/uploadfile', 'UploadController@UploadFile');
});

