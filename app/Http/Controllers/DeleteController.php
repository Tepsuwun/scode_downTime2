<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function delete_user(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
           $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $emp_id = $_POST['emp_id'];
                DB::select("DELETE FROM `user` WHERE `user`.`emp_id` = '$emp_id'");
                DB::select("DELETE FROM `employee` WHERE `employee`.`emp_id` = $emp_id;");
                
               $report = array('message' => 'ลบ user สำเร็จ (Delete user)', 'status' => $status);
            }else {
                $status = 401;
                $report = array('message' => 'กรุณากรอกข้อมูล (not data)', 'status' => $status);
            }
        }else{
            $status = 401;
            $report = array('message' => 'ไม่สามารถติอต่อฐานข้อมูลได้ (not database)', 'status' => $status);
        }
       return response()->json($report);
    }
}
