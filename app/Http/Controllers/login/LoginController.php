<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class LoginController extends Controller
{
    public function loginuser () {
        $_POST = json_decode(file_get_contents('php://input'),true);
        if(isset($_POST) && !empty($_POST)) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            // $username = '0822222222';
            // $password = '1234';
            $success = false;
            $status = "200";
            $message = "ไอดีหรือรหัสผ่านไม่ถูกต้อง";
            $user_status = 0;
            $emp_id = 0;
            $data = DB::select("SELECT * FROM `user` WHERE `user_id` LIKE '$username' AND `user_pass` LIKE '$password' ORDER BY `user_status` ASC");
            foreach ($data as $key => $value ){
                $success = true;
                $status = "200";
                $message = "รหัสผ่านถูกต้อง";
                $username = $data[0]->user_id;
                $password = $data[0]->user_pass;
                $user_status = $data[0]->user_status;
                $emp_id = $data[0]->emp_id;
            }
            $data2 = DB::select("SELECT `emp_data` FROM `employee` WHERE `emp_id` = $emp_id");
            // print_r ($data2);
            $emp_data = $data2[0]->emp_data;
            
        }else {
            $success = false;
            $status = "401";
            $message = "ไม่กรอกไอดีหรือรหัสผ่าน";
            $username = "";
            $password = "";
            $user_status = 2;
            $emp_id = 0;
            $emp_data = 0;
        }
        $report = array('success' => $success, 
                        'status' => $status,
                        'message' => $message,
                        'username' => $username,
                        // 'password' => $password,
                        'user_status' => $user_status,
                        'emp_id' => $emp_id,
                        'emp_data' => $emp_data);
        return response()->json($report);
    }
    public function loginadmin () {
        return response()->json("admin");
    }
}
