<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function user(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $data = DB::select("SELECT * FROM `user` WHERE `user_status` = '1' OR `user_status` = '2' ORDER BY `emp_id`");
            $report = array('data' => $data, 'status' => $status);
        }else{
            $status = 401;
            $data = [];
            $report = array('data' => $data, 'status' => $status);
        }
        return response()->json($report);
    }

    public function manager(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $data = DB::select("SELECT * FROM `user` WHERE `user_status` = '3' ORDER BY `emp_id`");
            $report = array('data' => $data, 'status' => $status);
        }else{
            $status = 401;
            $data = [];
            $report = array('data' => $data, 'status' => $status);
        }
        return response()->json($report);
    }

    public function alluser(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            // $user = DB::select("SELECT * FROM `user` WHERE `user_status` = 1 OR `user_status` = 2 ORDER BY `emp_id` ASC");
            $user = DB::table('user')
            ->leftJoin('employee', 'user.emp_id', '=', 'employee.emp_id')
            ->select('employee.*', 'user.*')
            ->where('user.user_status', '<=', 2)
            ->orderBy('user.emp_id', 'ASC')
            ->get();
            $manager = [];
            foreach ($user as $key => $value) {
                $user_status = $user[$key]->user_status;
                $emp_nickname = $user[$key]->emp_nickname;
                $emp_name = $user[$key]->emp_name;
                $emp_lastname = $user[$key]->emp_lastname;
                $user[$key]->emp_name = "$emp_name $emp_lastname";
                
                switch ($user_status) {
                    case 1 :
                        $user[$key]->user_status = "Employee";
                        break;
                    case 2 :
                        $user[$key]->user_status = "Supervisor";
                        break;
                    case 3 :
                        $user[$key]->user_status = "Manager";
                        break;
                    case 4 :
                        $user[$key]->user_status = "Admin";
                        break;
                    default:
                        $user[$key]->user_status = "notuser";
                        break;
                }
                $emp_still = $user[$key]->emp_still;
                switch ($emp_still) {
                    case 0 :
                        $user[$key]->emp_still = "ไม่ทำงานแล้ว";
                        break;
                    case 1 :
                        $user[$key]->emp_still = "ยังทำงานอยู่";
                        break;
                    default:
                        $user[$key]->emp_still = "error";
                        break;
                }
            }
//            print_r ($data);
            $report = array('user' => $user, 'manager' => $manager, 'status' => $status);
        }else{
            $status = 401;
            $manager = [];
            $user = [];
            $report = array('user' => $user, 'manager' => $manager, 'status' => $status);
        }
        return response()->json($report);
    }

    public function all_register(){
        if(DB::connection()->getDatabaseName())
        {
            $status = 200;
            $users = DB::table('user')
            ->leftJoin('employee', 'user.emp_id', '=', 'employee.emp_id')
            ->select('employee.*', 'user.user_status', 'user.user_id')
            ->where('user.user_status', '<=', 2)
            ->get();
            // print_r ($users);
            /*foreach ($users as $key => $value) {
                print_r ($value);
                echo "<hr>";
            }*/
//            print_r ($data);
            $manager = "อ่านข้อมูลสำเร็จ";
            $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
        }else{
            $status = 401;
            $manager = [];
            $users = [];
            $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
        }
        return response()->json($report);
    }

    public function oneuser(){
        if(DB::connection()->getDatabaseName())
        {
            $_POST = json_decode(file_get_contents('php://input'),true);
            if(isset($_POST) && !empty($_POST)) {
                $status = 200;
                $emp_id = $_POST['emp_id'];
                $users = DB::table('user')
                ->leftJoin('employee', 'user.emp_id', '=', 'employee.emp_id')
                ->select('employee.*', 'user.user_status', 'user.user_id')
                ->where('user.emp_id', '=', $emp_id)
                ->get();
        
                
                $manager = "อ่านข้อมูลสำเร็จ";
                $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
            }else {
                $status = 401;
                $manager = "ไม่ได้รับค่า emp_id";
                $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
            }
        }else{
            $status = 401;
            $manager = [];
            $users = [];
            $report = array('users' => $users, 'manager' => $manager, 'status' => $status);
        }
        return response()->json($report);
    }
}
